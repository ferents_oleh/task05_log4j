package com.epam;

import com.epam.controller.UserController;
import com.epam.controller.impl.UserControllerImpl;
import com.epam.model.dao.UserDao;
import com.epam.model.dao.impl.UserDaoImpl;
import com.epam.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        logger.info("Initializing beans");
        UserDao userDao = new UserDaoImpl();
        UserController userController = new UserControllerImpl(userDao);

        logger.info("Initializing menu");
        logger.fatal("Sms test");
        new Menu(userController);
    }
}

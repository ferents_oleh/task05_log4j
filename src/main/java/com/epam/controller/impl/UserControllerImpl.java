package com.epam.controller.impl;

import com.epam.controller.UserController;
import com.epam.exceptions.FirstNameNotFoundException;
import com.epam.model.dao.UserDao;
import com.epam.model.domain.User;
import com.epam.service.UserFileService;
import com.epam.service.impl.UserFileServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class UserControllerImpl implements UserController {
    private UserDao userDao;

    public UserControllerImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    private static Logger logger = LogManager.getLogger(UserControllerImpl.class);

    @Override
    public void printAllUsers() {
        logger.debug("Printing all users");
        userDao
                .getAllUsers()
                .forEach(System.out::println);
    }

    @Override
    public void printAllUsersByFirstName() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter first name - ");
        String firstName = scanner.next();

        try {
            logger.debug("Printing all users by first name");
            userDao.getAllUsersByFirstName(firstName).forEach(System.out::println);
        } catch (FirstNameNotFoundException e) {
            System.out.println("Such first name not found");
        }
    }

    @Override
    public void addUser() {
        logger.debug("Getting user's values");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter user data:");

        System.out.print("First name - ");
        String firstName = scanner.next();

        System.out.print("Last name - ");
        String lastName = scanner.next();

        System.out.print("Age - ");
        int age = scanner.nextInt();

        System.out.print("Sex - ");
        String sex = scanner.next();

        logger.debug("Adding user");
        userDao.addUser(new User(
                firstName,
                lastName,
                age,
                sex
        ));
    }

    @Override
    public void saveUsersToFile() {
        Scanner scanner = new Scanner(System.in);
        logger.debug("Entering file name where users will be stored");
        System.out.print("Enter file name - ");
        String filepath = scanner.next();
        try (UserFileService userFileService = new UserFileServiceImpl()) {
            logger.debug("Saving users to file");
            userFileService.saveUsersToFile(filepath, userDao.getAllUsers());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

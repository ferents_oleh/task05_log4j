package com.epam.controller;

public interface UserController {
    void printAllUsers();

    void printAllUsersByFirstName();

    void addUser();

    void saveUsersToFile();
}

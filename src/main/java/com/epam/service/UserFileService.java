package com.epam.service;

import com.epam.model.domain.User;

import java.util.List;

public interface UserFileService extends AutoCloseable {
    void saveUsersToFile(String filename, List<User> users);
}

package com.epam.service.impl;

import com.epam.model.domain.User;
import com.epam.service.UserFileService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class UserFileServiceImpl implements UserFileService {
    @Override
    public void saveUsersToFile(String filename, List<User> users) {
        try(FileOutputStream f = new FileOutputStream(new File(filename));
            ObjectOutputStream o = new ObjectOutputStream(f)) {
            users.forEach(u -> {
                try {
                    o.writeObject(u);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() throws Exception {
        System.out.println("End");
    }
}

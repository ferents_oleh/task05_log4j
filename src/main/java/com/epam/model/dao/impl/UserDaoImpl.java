package com.epam.model.dao.impl;

import com.epam.exceptions.FirstNameNotFoundException;
import com.epam.exceptions.ListTooLargeException;
import com.epam.model.dao.UserDao;
import com.epam.model.domain.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserDaoImpl implements UserDao {
    private List<User> users;

    public UserDaoImpl() {
        init();
    }

    private void init() {
        users = new ArrayList<>();

        List<User> data = new ArrayList<User>() {
            {
                add(new User(
                        "John",
                        "Jackson",
                        26,
                        "male"
                ));
                add(new User(
                        "Nora",
                        "Case",
                        28,
                        "female"
                ));
                add(new User(
                        "Jalen",
                        "Coleman",
                        18,
                        "male"
                ));
                add(new User(
                        "Jamie",
                        "Beltran",
                        23,
                        "male"
                ));
            }
        };

        users.addAll(data);
    }

    private void checkListSize() {
        if (users.size() >= 5) {
            throw new ListTooLargeException("List can't exceed more than 5 items");
        }
    }

    @Override
    public List<User> getAllUsers() {
        return users;
    }

    @Override
    public List<User> getAllUsersByFirstName(String firstName) throws FirstNameNotFoundException {
        List<User> result = users
                .stream()
                .filter(fn -> fn.getFirstName().equals(firstName))
                .collect(Collectors.toList());
        if (result.isEmpty()) {
            throw new FirstNameNotFoundException("Name is empty");
        }
        return result;
    }

    @Override
    public void addUser(User user) {
        users.add(user);
        try {
            checkListSize();
        } catch (ListTooLargeException e) {
            System.out.println("Can't add user: " + e.getMessage());
        }
    }
}
